create database Well_Being_Pharmacy;


create table if not exists Department(
dept_no int (20) not null,
primary key (dept_no),
dept_name char (50) not null
);

create table if not exists staff(
staff_id int (20)not null,
primary key (staff_id),
first_name char (50) not null,
last_name char (50) not null,
position char (50),
pps_no varchar (30) not null,
start_date date,
dept_no int (20) not null,
constraint fk_dept_no foreign key (dept_no)
references department (dept_no)
);


create table if not exists Customers(
cust_no int (30) auto_increment not null,
primary key (cust_no),
cust_name char (50),
cust_add varchar (50),
cust_phone varchar (30),
cust_type varchar (50) not null,
private_id int (30),
full_card_id int (30),
gp_visit_card_id int (30),
check (cust_type = 'private' or 'full_card' or 'gp_visit')
);

create table if not exists orders(
order_no int (20) not null,
primary key (order_no),
order_type char (50) not null,
price decimal (8,2),
prod_name varchar (50),
quantity int (30),
staff_id1 int (20) not null,
cust_no1 int (30) not null,
constraint fk_staff_id1 foreign key (staff_id1)
references staff (staff_id),
constraint fk_cust_no1 foreign key (cust_no1)
references customers (cust_no),
check (order_type = 'medical' or 'retail' or 'beauty')
);


create table if not exists Prescriptions(
presc_no int (20) auto_increment not null,
primary key (presc_no),
presc_name varchar (50),
medcard varchar (30),
issue_date date,
cust_no2 int (30) not null,
constraint fk_cust_no2 foreign key (cust_no2)
references customers (cust_no),
check (medcard = 'yes' or 'no')
);


create table if not exists beauty_club(
bc_reg_id int (30) auto_increment not null,
primary key (bc_reg_id),
beauty_card_no int (20) not null,
issue_date date, 
expiry_date date,
cust_no3 int (30) not null,
constraint fk_cust_no3 foreign key (cust_no3)
references customers (cust_no)
);


create table if not exists make_up(
mu_id int (20) not null,
primary key (mu_id),
mu_name char (30)
);


create table if not exists stockists(
bc_name char (30),
bc_reg_id int (30) auto_increment not null,
mu_id int (20) not null,
primary key (bc_reg_id, mu_id),
bc_reg_id2 int (30) not null,
mu_id2 int (20) not null,
constraint fk_bc_reg_id2 foreign key (bc_reg_id2)
references beauty_club (bc_reg_id),
constraint fk_mu_id2 foreign key (mu_id2)
references make_up (mu_id)
);


insert into department(dept_no, dept_name)
values (10, 'Pharmacy');

insert into department(dept_no, dept_name)
values (20, 'Retail');

insert into department(dept_no, dept_name)
values (30, 'Beauty Club');

insert into department(dept_no, dept_name)
values (40, 'Cleaners');

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('100', 'Mary', 'Murphy', 'Manager', '8011921J', '2013-01-12', 10);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('101', 'Anna', 'Dowling', 'Manager', '8011902B', '2013-01-14', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('102', 'Barry', 'Griffin', 'Manager', '8112467H', '2013-01-10', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('103', 'Gavin', 'Wilson', 'Manager', '8785465K', '2013-01-09', 40);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('104', 'Ciara', 'Reddie', 'Retail Operative', '8000052G', '2013-02-14', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('105', 'Vickie', 'Kiely', 'Retail Operative', '82223335K', '2013-02-18', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('106', 'Denise', 'Cheasty', 'Retail Operative', '8415339P', '2013-02-17', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('107', 'Liana', 'Moynan', 'Retail Operative', '8555672K', '2013-03-20', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('108', 'Lynn', 'Harding', 'Retail Operative', '8495612K', '2013-04-15', 20);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('109', 'Cathy', 'Dowling', 'Beautician', '9055241M', '2013-05-12', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('110', 'Miranda', 'Kerr', 'Beautician', '9795142K', '2013-05-30', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('111', 'Cara', 'Delevigne', 'Beautician', '9445613F', '2013-06-16', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('112', 'Beyonce', 'Knowles', 'Beautician', '9456312L', '2013-07-09', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('113', 'Keira', 'Power', 'Beautician', '8965431W', '2013-08-04', 30);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('114', 'Michael', 'Phelan', 'Pharmacist', '8975543D', '2013-03-03', 10);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('115', 'Emma', 'Dennehy', 'Pharmacist', '9631542R', '2013-03-16', 10);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('116', 'John', 'Frances', 'Pharmacist', '8312564F', '2013-01-01', 10);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('117', 'Berna', 'Moriarty', 'Cleaner', '8111222U', '2013-01-30', 40);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('118', 'June', 'Guinness', 'Cleaner', '8555461S', '2013-02-19', 40);

insert into staff(staff_id, first_name, last_name, position, pps_no, start_date, dept_no)
values ('119', 'Owen', 'Donovan', 'Pharmaceutical Technician', '8446312F', '2013-01-08', 10);

insert into customers(cust_name, cust_add, cust_phone, cust_type, private_id)
values ('Maria Neill', '5 Roselawn Tramore Co. Waterford', '0861064568', 'Private', 2651130);

insert into customers(cust_name, cust_add, cust_phone, cust_type, private_id)
values ('Mark Doyle', '5 Faranrae Tramore Co. Waterford', '0874563222', 'Private', 2651131);

insert into customers(cust_name, cust_add, cust_phone, cust_type, private_id)
values ('Craig Wilmott', '23 Crobally Heights Tramore Co. Waterford', '0861025213', 'Private', 2651132);

insert into customers(cust_name, cust_add, cust_phone, cust_type, private_id)
values ('Aaron Sugrue', '5 Main Street Kilkenny', '0867778888', 'Private', 2651133);

insert into customers(cust_name, cust_add, cust_phone, cust_type, private_id)
values ('Pamela Scott', '18 Palm Springs Waterford', '0861025213', 'Private', 2651134);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Julie Nolan', '14 Monvoy Valley Tramore Co. Waterford', '0861016565', 'Full_card', 2651135);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Orla Murphy', '2 Friars walk Waterford', '0851523311', 'Full_card', 2651136);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Patsy Dolan', '20 Marian Park Dunmore Co. Waterford', '0831478963', 'Full_card', 2651137);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Matt Blanc', '6 Ocean Drive Wexford', '0851063214', 'Full_card', 2651138);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Frances Meagher', '21 Westbrook Tramore Co. Waterford', '0871102365', 'Full_card', 2651139);

insert into customers(cust_name, cust_add, cust_phone, cust_type, full_card_id)
values ('Rebecca Black', '5 Cluain Mhor Tramore Co. Waterford', '0851084567', 'Full_card', 2651140);

insert into customers(cust_name, cust_add, cust_phone, cust_type, gp_visit_card_id)
values ('Zoe Williams', '5 Cuil Beag Tramore Co. Waterford', '0854447778', 'gp_visit', 2651141);

insert into customers(cust_name, cust_add, cust_phone, cust_type, gp_visit_card_id)
values ('Frank Mullins', '8 John Street Waterford', '0895694589', 'gp_visit', 2651142);

insert into customers(cust_name, cust_add, cust_phone, cust_type, gp_visit_card_id)
values ('Paddy Casey', '7 Apple Market Waterford', '0851456565', 'gp_visit', 2651143);

insert into customers(cust_name, cust_add, cust_phone, cust_type, gp_visit_card_id)
values ('Sarah Walsh', '4 Larchville Waterford', '0895694589', 'gp_visit', 2651144);

insert into customers(cust_name, cust_add, cust_phone, cust_type, gp_visit_card_id)
values ('Jason Keane', '4 Roselawn Tramore Co. Waterford', '0821427878', 'gp_visit', 2651145);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (110, 'medical', 15.99, 'Zirtek', 2, 115, 1);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (111, 'medical', 8.50, 'SolpaSinus', 2, 116, 3);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (112, 'medical', 2.50, 'Codinex', 1, 115, 13);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (113, 'medical', 2.50, 'Motilium', 1, 116, 14);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (114, 'medical', 5.00, 'Pirotin', 2, 115, 8);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (115, 'retail', 20.99, 'Tangle Teaser', 2, 104, 2);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (116, 'retail', 14.80, 'Yankee Candle', 3, 105, 4);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (117, 'retail', 40.00, 'Chanel NO5', 1, 106, 5);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (118, 'retail', 7.99, 'Photo Frame', 1, 107, 2);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1)
values (119, 'beauty', 25.00, 'MAC Blusher', 1, 110, 5);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1 )
values (120, 'beauty', 250.00, 'Clarins Set', 2, 113, 4);

insert into orders(order_no, order_type, price, prod_name, quantity, staff_id1, cust_no1 )
values (121, 'beauty', 300.00, 'Smashbox', 15, 112, 5);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Julie Nolan', 'Yes', '2014-05-01', 6);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Orla Murphy', 'Yes', '2014-05-15', 7);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Mark Doyle', 'No', '2014-05-11', 2);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Craig Wilmott', 'No', '2014-05-04', 3);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Zoe Williams', 'Yes', '2014-04-17', 12);

insert into prescriptions(presc_name, medcard, issue_date, cust_no2)
values('Frank Mullins', 'Yes', '2014-04-04', 13);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110234, '2013-08-08', '2014-08-08', 5);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110235, '2013-09-09', '2014-09-09', 1);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110236, '2013-10-10', '2014-10-10', 2);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110237, '2013-11-11', '2014-11-11', 11);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110238, '2013-12-12', '2014-12-12', 12);

insert into beauty_club(beauty_card_no, issue_date, expiry_date, cust_no3)
values(110239, '2013-07-07', '2014-07-07', 3);

insert into make_up(mu_id, mu_name)
values(1, 'MAC');

insert into make_up(mu_id, mu_name)
values(2, 'Smashbox');

insert into make_up(mu_id, mu_name)
values(3, 'Clarins');

insert into make_up(mu_id, mu_name)
values(4, 'Clinique');

insert into make_up(mu_id, mu_name)
values(5, 'Urban Decay');

insert into make_up(mu_id, mu_name)
values(6, 'Lancome');


insert into stockists(bc_name, mu_id, bc_reg_id2, mu_id2)
values('Well_Being_Pharmacy Cork', 1, 1, 1);


insert into stockists(bc_name, mu_id, bc_reg_id2, mu_id2)
values('Well_Being_Pharmacy Dublin', 2, 2, 2);

insert into stockists(bc_name, mu_id, bc_reg_id2, mu_id2)
values('Well_Being_Pharmacy Belfast', 3, 3, 3);

insert into stockists(bc_name, mu_id, bc_reg_id2, mu_id2)
values('Well_Being_Pharmacy Galway', 4, 4, 4);

insert into stockists(bc_name, mu_id, bc_reg_id2, mu_id2)
values('Well_Being_Pharmacy Limerick',5 , 5, 5);


create view Prescription_Holders as
select cust_no, cust_name, cust_type
from customers;

create view Beauty_Data as
select bc_reg_id, beauty_card_no
from beauty_club;

create view make_up_data as
select mu_name
from make_up;

create view stock_info as
select bc_name, mu_id
from stockists;

create view medical_orders as
select order_no, order_type, prod_name
from orders;

create view customer_data as
select cust_name, cust_type, cust_phone
from customers;

create view staff_data as
select first_name, last_name, position, dept_no
from staff;

create user pharmacist
identified by 'pharma101';

SELECT PASSWORD ('pharma101') AS encrypt_password;

grant select, update, insert
on well_being_pharmacy.Prescription_Holders
to pharmacist;

grant select, insert, update, delete
on well_being_pharmacy.medical_orders
to pharmacist;

create user beauticians
identified by 'beauty101';

select password ('beauty101') AS encrypt_password;

grant select, insert, update
on well_being_pharmacy.Beauty_Data
to beauticians;

grant select
on well_being_pharmacy.stock_info
to beauticians;

grant select, insert, update, delete
on well_being_pharmacy.make_up_data
to beauticians;

create user retails_ops
identified by 'retail101';

select password ('retail101') AS encrypt_password;

grant select
on well_being_pharmacy.stock_info
to retails_ops;

grant select, insert, update
on well_being_pharmacy.customer_data
to retails_ops;

create user all_staff
identified by 'staff101';

select password ('staff101') AS encrypt_password;

grant select
on well_being_pharmacy.staff_data
to all_staff;

create unique index ppsn
on staff (pps_no);



















